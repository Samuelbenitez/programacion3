﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PaginaWeb2.Models
{
    public class Form
    {
        [MaxLength(30), MinLength(4)]
        [RegularExpression(@"^[A-Z a-z0-9ÑñáéíóúÁÉÍÓÚ\\-\\_]+$")]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")]
        [Display(Name = "Correo")]
        public string Correo { get; set; }

        [StringLength(20)]
        [RegularExpression(@"/^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]*$/")]
        [Display(Name = "Asunto")]
        public string Asunto { get; set; }
        
        [MaxLength(100), MinLength(10)]
        [RegularExpression(@"/^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]*$/")]
        [Display(Name = "Mensaje")]
        public string Mensaje { get; set; }

    }
}