﻿using PaginaWeb2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.WebPages;

namespace PaginaWeb2.Controllers
{
    public class ContactoController : Controller
    {
        // GET: Contacto
        public ActionResult Contacto()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Validacion(Form form)
        {

            if (!form.Nombre.IsEmpty() && !form.Correo.IsEmpty() && !form.Asunto.IsEmpty() && !form.Mensaje.IsEmpty())
            {
                ViewBag.mensaje = "Informacion Enviada";

            }

            object data = ViewBag.mensaje;

            return Json(data,JsonRequestBehavior.AllowGet);
        }

    }
}