﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDeNegocio
{
    public static class Negocio
    {

        public static List<CNProducto> ObtenerProducto()
        {



            return new List<CNProducto> {


                 new CNProducto
                {
                    ID ="1",
                    Imagen="/Img/lenovocompu.png",
                    Nombre="Notebook 2 en 1 Lenovo Flex 5 14 AMD Ryzen 16 GB",
                    Precio="$189.600,00"

                },
                new CNProducto
                {
                    ID ="2",
                    Imagen="/Img/6f948059428debdc23495a5060a3.jpg",
                    Nombre="Smart TV LED Philips 58 4K Ultra HD 58PUD6654/77",
                    Precio="$74.999,00"

                },
                 new CNProducto
                {
                    ID ="3",
                    Imagen="/Img/e4a1148ccb3556c11d09b954a58d.jpg",
                    Nombre="Celular Libre Samsung Galaxy S20 SM-G980F Gris 128 GB",
                    Precio="$97.999,00"

                },
                new CNProducto
                {
                    ID ="4",
                    Imagen="/Img/samsungnote10.png",
                    Nombre="Celular Libre Samsung Galaxy Note 10+ Plateado SM-N975FZSLARO",
                    Precio="$114.999,00"

                },
                 new CNProducto
                {
                    ID ="5",
                    Imagen="/Img/e6dae8684b6e3d9b36aea07c891c.jpg",
                    Nombre="Smart TV LED Philips 70 4K Ultra HD 70PUG6774/77",
                    Precio="$149.999,00"

                },
                new CNProducto
                {
                    ID ="6",
                    Imagen="/Img/b9a8ae9aa5cef096f7af6715edf8.jpg",
                    Nombre="Escritorio Melamina Centro Estant SC109Wr",
                    Precio="$3.578,00"

                }
            };


        }


        public static List<CNCategoria> ObtenerCategorias()
        {



            return new List<CNCategoria> {


                 new CNCategoria
                {
                    ID ="1",
                    categoria="Categoria",
                    SubCategoria={}

                },
                new CNCategoria
                {
                    ID ="2",
                    categoria="TECNOLOGIA",
                    SubCategoria={"CELULARES Y TABLES","INFORMATICA","VIDEOJUEGOS"}

                },
                new CNCategoria
                {
                    ID ="3",
                    categoria="ELECTRODOMESTICOS",
                    SubCategoria={"TELEVISORES Y VIDEOS","ELECTRODOMESTICOS PEQUEÑOS"}

                },
                new CNCategoria
                {
                    ID ="4",
                    categoria="CASA Y JARDIN",
                    SubCategoria={"MUEBLES","LIVING COMEDOR Y DECO"}

                }

            };

        }
    }
}
