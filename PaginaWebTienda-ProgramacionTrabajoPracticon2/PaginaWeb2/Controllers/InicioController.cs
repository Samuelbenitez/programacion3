﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.UI.WebControls;
using CapaDeNegocio;
using PaginaWeb2.Models;

namespace PaginaWeb2.Controllers
{
    public class InicioController : Controller
    {
        // GET: Inicio
        public ActionResult Inicio()
        {


            var respuesta = Negocio.ObtenerProducto();
            var respuestaCate = Negocio.ObtenerCategorias();

            var miModelo = new InicioModel();
            miModelo.Producto = respuesta.Select(x => new Producto
            {
                ID = x.ID,
                Nombre = x.Nombre,
                Imagen = x.Imagen,
                Precio = x.Precio
            }).ToList();
            miModelo.Categoria= respuestaCate.Select(x => new Categoria
            {
                ID = x.ID,
                categoria=x.categoria,
                SubCategoria=x.SubCategoria
            }).ToList();
            
            return View(miModelo);



            /*
                        var respuesta = Negocio.ObtenerProducto();

                        var Lista_producto = respuesta.Select(x => new Producto
                        {
                            ID = x.ID,
                            Nombre = x.Nombre,
                            Imagen = x.Imagen,
                            Precio = x.Precio
                        }).ToList();

                        return View(Lista_producto);*/

            //var res = Negocio.ObtenerCategorias();

            //var Lista_categoria = res.Select(x => new Categoria
            //{
            //    ID = x.ID,
            //    categoria = x.categoria,
            //    subCateoria = x.subCategoria

            //}).ToList();








        }
    }
}