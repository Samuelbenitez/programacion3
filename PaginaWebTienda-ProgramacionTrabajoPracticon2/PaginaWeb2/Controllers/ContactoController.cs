﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PaginaWeb2.Controllers
{
    public class ContactoController : Controller
    {
        // GET: Contacto
        public ActionResult Contacto()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Contacto(string Nombre, string Correo, string Asunto,string mensaje)
        {
            /* var nombre = Nombre;
             var correo = Correo;
             var asunto = Asunto;
             var Mensaje = mensaje;*/

            ViewBag.Minombre = Nombre;
            ViewBag.Micorreo = Correo;
            ViewBag.Miasunto= Asunto;
            ViewBag.Mimensaje = mensaje;

            if (ViewBag.Minombre !=null || ViewBag.Micorreo != null || ViewBag.Miasunto != null || ViewBag.Mimensaje !=null )
            {

                ViewBag.alerta = "informacion enviada";
            }

            return View();
        }

    }
}