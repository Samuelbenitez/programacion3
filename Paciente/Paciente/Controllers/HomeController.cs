﻿using CapaDeNegocio;
using Pacientes.Models;
using System.Linq;
using System.Web.Mvc;

namespace Pacientes.Models
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            var respuesta = Hospital.ObtenerPacientes();

            var lista_pacientes = respuesta.Select(x => new Paciente
            {
                ID = x.ID,
                Nombre = x.Nombre,
                Apellido = x.Apellido,
                Edad = x.Edad,
                Domicilio = x.Domicilio
            }).ToList();

            return View(lista_pacientes);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}