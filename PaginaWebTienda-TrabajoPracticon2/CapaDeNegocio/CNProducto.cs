﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDeNegocio
{
    public class CNProducto
    {
        public string ID { get; set; }
        public string Imagen { get; set; }
        public string Nombre { get; set; }
        public string Precio { get; set; }
    }
}
