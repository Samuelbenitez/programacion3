﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDeNegocio
{
    public class CNCategoria
    {
        public CNCategoria()
        {
            SubCategoria = new List<string>();
        }
        public string ID { get; set; }
        public string categoria { get; set; }
        public List<string> SubCategoria { get; set; }


    }
}
